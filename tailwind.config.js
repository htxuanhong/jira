module.exports = {
  important: true,
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./src/**/**/*.{js,jsx,ts,tsx}",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  theme: {
    extend: {
      height: {
        715: "712px",
      },
      width: {
        715: "700px",
      },
    },
  },
  plugins: [require("tw-elements/dist/plugin")],
};
