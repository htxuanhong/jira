import { combineReducers } from "redux";
import { drawerReducer } from "./DrawerBugReducer";
import { ProjectReducer } from "./ProjectReducer";
import { spinnerReducer } from "./spinnerReducer";
import { taskReducer } from "./taskReducer";

export const rootReducer = combineReducers({
  drawerReducer,
  ProjectReducer,
  spinnerReducer,
  taskReducer,
});
