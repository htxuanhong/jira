import Main from "../component/Main";
import LayoutThem from "../HOC/LayoutThem";
import CreateProject from "../page/CreateProject/CreateProject";

import LoginPage from "../page/LoginPage/LoginPage";
import NotFound from "../page/NotFound/NotFound";
import ProjectDetail from "../page/ProjectDetailCyber/ProjectDetailCyber";
import ProjectManagement from "../page/ProjectManagement/ProjectManagement";

// mảng chứa các object qua app.js map ra
export const userRoutes = [
  {
    path: "/",
    component: <LayoutThem Component={ProjectManagement} />,
    exact: true, // không hiểu
    isUseLayout: true,
  },

  {
    path: "/CreateProject",
    component: <LayoutThem Component={CreateProject} />,
    isUseLayout: true,
  },
  {
    path: "/ProjectManagement",
    component: <LayoutThem Component={ProjectManagement} />,
    isUseLayout: true,
  },
  {
    path: "/projectdetail/:projectId",
    component: <LayoutThem Component={ProjectDetail} />,
    isUseLayout: true,
  },

  {
    path: "/login",
    component: LoginPage,
  },
  {
    path: "*",
    component: NotFound,
  },
];
