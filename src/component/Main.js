import React from "react";

export default function Main(props) {
  const { projectDetail } = props;

  const renderAvatar = () => {
    return projectDetail?.members?.map((user, index) => {
      return (
        <div key={index} className="avatar">
          <img src={user.avatar} alt={user.avatar} />
        </div>
      );
    });
  };

  const renderCardTaskList = () => {
    return projectDetail?.lstTask.map((taskListDetail, index) => {
      return (
        <div
          key={index}
          className="card"
          style={{ width: "17rem", height: "auto" }}
        >
          <div className="card-header">{taskListDetail.statusName}</div>

          {taskListDetail.lstTaskDeTail.map((task, index) => {
            return (
              <ul key={index} className="list-group list-group-flush">
                <li
                  className="list-group-item"
                  data-toggle="modal"
                  data-target="#infoModal"
                  style={{ cursor: "pointer" }}
                >
                  <h4 className="font-weight-bold">{task.taskName}</h4>
                  <div className="block" style={{ display: "flex" }}>
                    <div className="block-left">
                      <p className="text-danger">
                        {task.priorityTask.priority}
                      </p>
                    </div>
                    <div className="block-right">
                      <div className="avatar-group" style={{ display: "flex" }}>
                        {task.assigness.map((mem, index) => {
                          return (
                            <div className="avatar " key={index}>
                              <img src={mem.avatar} alt="avt" />
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </li>

                <li className="list-group-item">Vestibulum at eros</li>
              </ul>
            );
          })}
        </div>
      );
    });
  };
  return (
    <div className="main">
      <div className="header">
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb" style={{ backgroundColor: "white" }}>
            <li className="breadcrumb-item">Project</li>
            <li className="breadcrumb-item">CyberLearn</li>
            <li className="breadcrumb-item">Project management</li>
            <li className="breadcrumb-item active" aria-current="page">
              {projectDetail?.projectName}
            </li>
          </ol>
        </nav>
      </div>
      <h3 className="uppercase text-2xl">{projectDetail?.projectName}</h3>
      <section>
        <div
          dangerouslySetInnerHTML={{
            __html: projectDetail?.description,
          }}
        />
      </section>
      <div className="info" style={{ display: "flex" }}>
        <div className="search-block">
          <input className="search" />
          <i className="fa fa-search" />
        </div>
        <div className="avatar-group" style={{ display: "flex" }}>
          {renderAvatar()}
        </div>
        <div style={{ marginLeft: 20 }} className="text">
          Only My Issues
        </div>
        <div style={{ marginLeft: 20 }} className="text">
          Recently Updated
        </div>
      </div>
      <div className="content" style={{ display: "flex" }}>
        {renderCardTaskList()}
      </div>
    </div>
  );
}
