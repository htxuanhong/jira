import { DeleteOutlined } from "@ant-design/icons";
import { message, Popconfirm } from "antd";
import { useDispatch } from "react-redux";
import { setHideSpinner, setShowSpinner } from "../../redux/action/action";
import { projectService } from "../../service/projectService";

const DeleteProject = (props) => {
  const dispatch = useDispatch();
  const confirm = (e) => {
    console.log(e);
    dispatch(setShowSpinner());
    projectService
      .deleteProject(props.projectId)
      .then((res) => {
        dispatch(setHideSpinner());
        console.log(res);
        message.success("Xóa thành công");

        props.onSuccess?.();
      })
      .catch((err) => {
        dispatch(setHideSpinner());
        console.log(err);
      });
  };

  return (
    <Popconfirm
      title="Bạn chắc chắn muốn xóa chứ?"
      onConfirm={confirm}
      okText="Đồng ý"
      cancelText="Hủy"
    >
      <button
        className="bg-red-500 hover:bg-red-800  text-white rounded-md  hover:text-white"
        style={{ fontSize: 14 }}
      >
        <DeleteOutlined className="p-2" />
      </button>
    </Popconfirm>
  );
};

export default DeleteProject;
