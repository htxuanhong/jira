import { Editor } from "@tinymce/tinymce-react";
import { message, Select, Slider } from "antd";
import { withFormik } from "formik";
import { debounce } from "lodash";
import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import * as Yup from "yup";
import { closeDrawer, setSubmitCreateTask } from "../../redux/action/action";
import { priorityService } from "../../service/priorityService";
import { projectService } from "../../service/projectService";
import { statusService } from "../../service/statusService";
import { taskTypeService } from "../../service/taskTypeService";
import { userService } from "../../service/userService";

const { Option } = Select;
const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

function FormCreateTask(props) {
  const [size, setSize] = useState("middle");
  const { values, handleChange, setFieldValue, handleSubmit } = props;
  const [timeTracking, setTimeTracking] = useState({
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
  });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setSubmitCreateTask(handleSubmit));
  }, []);

  const [allProject, setAllProject] = useState();
  const [taskType, setTaskType] = useState();
  const [priority, setPriority] = useState();
  const [user, setUser] = useState([]);
  const [arrStatus, setArrStatus] = useState([]);

  useEffect(() => {
    statusService
      .getAllStatus()
      .then((result) => {
        setArrStatus(result.data.content);
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    projectService
      .getListProject()
      .then((res) => {
        console.log(res);
        setAllProject(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    taskTypeService
      .getTaskType()
      .then((res) => {
        console.log(res);
        setTaskType(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    priorityService
      .getPriority()
      .then((res) => {
        console.log(res);
        setPriority(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const searchDelayAction = debounce((keyword) => {
    userService
      .getUser(keyword)
      .then((res) => {
        console.log(res.data.content);
        if (Array.isArray(res.data.content)) {
          setUser(res.data.content);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, 500);

  console.log("user", user);
  const userOption = user?.map((item, index) => {
    return { value: item.userId, label: item.name };
  });

  return (
    <form className="container" onSubmit={handleSubmit}>
      <div className="form-group">
        <label className="font-bold">Project</label>
        <select
          name="projectId"
          className="form-control"
          onChange={handleChange}
        >
          {allProject?.map((project, index) => {
            return (
              <option value={project.id} key={index}>
                {project.projectName}
              </option>
            );
          })}
        </select>
      </div>

      <div className="form-group">
        <p className="font-bold">Task name</p>
        <input
          className="form-control"
          onChange={handleChange}
          name="taskName"
          placeholder="Task name"
        />
      </div>
      <div className="form-group">
        <p className="font-bold">Status ID</p>
        <select
          className="form-control"
          onChange={handleChange}
          name="statusID"
          placeholder="Status ID"
        >
          {arrStatus.map((item, index) => {
            return (
              <option key={index} value={item.statusId}>
                {item.statusName}
              </option>
            );
          })}
        </select>
      </div>
      <div className="form-group">
        <div className="row">
          <div className="col-6">
            <label className="font-bold">Priority</label>
            <select
              name="priorityId"
              className="form-control"
              onChange={handleChange}
            >
              {priority?.map((priority, index) => {
                return (
                  <option key={index} value={priority.priorityId}>
                    {priority.priority}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="col-6">
            <label className="font-bold">Task type</label>
            <select
              name="typeId"
              className="form-control"
              onChange={handleChange}
            >
              {taskType?.map((taskType, index) => {
                return (
                  <option key={index} value={taskType.id}>
                    {taskType.taskType}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>
      <div className="form-group">
        <div className="row">
          <div className="col-6">
            <label className="font-bold">Assignees</label>
            <Select
              mode="multiple"
              size={size}
              placeholder="Please select"
              optionFilterProp="label"
              options={userOption}
              onChange={(values) => {
                setFieldValue("listUserAsign", values);
              }}
              onSearch={searchDelayAction}
              style={{ width: "100%" }}
            >
              {children}
            </Select>
            <div className="row" style={{ marginTop: "10px" }}>
              <div className="col-12">
                <label className="font-bold mt-4">Original estimate</label>
                <input
                  type="number"
                  defaultValue="0"
                  min="0"
                  className="form-control "
                  name="originalEstimate"
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
          <div className="col-6">
            <label className="font-bold">Time tracking</label>
            <Slider
              defaultValue={30}
              value={timeTracking.timeTrackingSpent}
              max={
                Number(timeTracking.timeTrackingSpent) +
                Number(timeTracking.timeTrackingRemaining)
              }
            />
            <div className="row">
              <div
                className="col-6 text-left font-bold"
                style={{ color: "#2c7da0" }}
              >
                {timeTracking.timeTrackingSpent}h logged
              </div>
              <div
                className="col-6 text-right font-bold"
                style={{ color: "#2c7da0" }}
              >
                {timeTracking.timeTrackingRemaining}h remainning
              </div>
            </div>
            <div className="row " style={{ marginTop: "5px" }}>
              <div className="col-6">
                <label className="font-bold">Time spent</label>
                <input
                  type="number"
                  defaultValue="0"
                  min="0"
                  className="form-control "
                  name="timeTrackingSpent"
                  onChange={(e) => {
                    setTimeTracking({
                      ...timeTracking,
                      timeTrackingSpent: e.target.value,
                    });
                    setFieldValue("timeTrackingSpent", e.target.value);
                  }}
                />
              </div>
              <div className="col-6">
                <label className="font-bold">Time remaining</label>
                <input
                  defaultValue="0"
                  min="0"
                  type="number"
                  className="form-control "
                  name="timeTrackingRemaining"
                  onChange={(e) => {
                    setTimeTracking({
                      ...timeTracking,
                      timeTrackingRemaining: e.target.value,
                    });
                    setFieldValue("timeTrackingRemaining", e.target.value);
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="form-group">
        <label className="font-bold">Description</label>
        <Editor
          name="description"
          init={{
            selector: "textarea#myTextArea",
            height: 400,
            menubar: false,
            plugins: [
              "advlist autolink lists link image charmap print preview anchor",
              "searchreplace visualblocks code fullscreen",
              "insertdatetime media table paste code help wordcount",
            ],
            toolbar:
              "undo redo | formatselect | " +
              "bold italic backcolor | alignleft aligncenter " +
              "alignright alignjustify | bullist numlist outdent indent | " +
              "removeformat | help",
          }}
          onEditorChange={(content, editor) => {
            setFieldValue("description", content);
          }}
        />
      </div>
    </form>
  );
}

const frmCreateTask = withFormik({
  mapPropsToValues: (props) => {
    // const { projectEdit } = props;

    return {
      listUserAsign: [],
      taskName: "",
      description: "",
      statusId: 1,
      originalEstimate: 0,
      timeTrackingSpent: 0,
      timeTrackingRemaining: 0,
      projectId: 0,
      typeId: 0,
      priorityId: 0,
    };
  },
  validationSchema: Yup.object().shape({}),
  handleSubmit: (values, { props }) => {
    projectService
      .createTask(values)
      .then((res) => {
        console.log(res);
        message.success("Thêm task thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data.content);
      });
  },
  displayName: "createTaskFrom",
})(FormCreateTask);

const mapStateToProps = (state) => ({});

const mapDispatchToProps = () => (dispatch) => {
  return {
    closeDrawerAction: () => dispatch(closeDrawer()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(frmCreateTask);
