import axios from "axios";

export const taskTypeService = {
  getTaskType: () => {
    return axios({
      method: "GET",
      url: `https://casestudy.cyberlearn.vn/api/TaskType/getAll`,
    });
  },
};
