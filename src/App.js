import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import SpinnerComponent from "./component/SpinnerComponent/SpinnerComponent";
import DrawerBugs from "./HOC/DrawerBugs/DrawerBugs";
import { userRoutes } from "./routes/userRoutes";

function App() {
  return (
    <div classname="App">
      <SpinnerComponent />
      <DrawerBugs />
      {/* <Route path="/" element={<HomePage />} />
        <Route path="/Login" element={<LoginPage />} /> */}
      <BrowserRouter>
        {" "}
        <Switch>
          {" "}
          {userRoutes.map((params, i) => {
            if (params.isUseLayout) {
              return (
                <Route
                  key={i}
                  exact={params.exact} // chỉ dùng cho trang home component={params.component}
                  path={params.path}
                  render={() => {
                    return params.component;
                  }}
                />
              );
            } else {
              return (
                <Route
                  key={i}
                  exact={params.exact} // chỉ dùng cho trang home
                  path={params.path}
                  component={params.component}
                />
              );
            }
          })}{" "}
        </Switch>{" "}
      </BrowserRouter>
    </div>
  );
}

export default App;
