import React from "react";
import Menu from "../component/Menu";
import Sildebar from "../component/Sildebar";

export default function LayoutThem({ Component }) {
  return (
    <div className="jira">
      <Sildebar />
      <Menu />

      <Component />
    </div>
  );
}
