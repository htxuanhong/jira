import React from "react";

import Fomlogin from "./Fomlogin/Fomlogin";

export default function LoginPage() {
  return (
    <div className="absolute w-screen">
      <img
        src={require("../../assets/img/login.avif")}
        alt=""
        className="w-screen h-screen absolute"
      />
      <div className="relative w-1/3 left-1/3 top-10 bg-white rounded-xl ">
        <Fomlogin />
      </div>
      <div className="text-white relative top-20 pl-96 ml-52">
        © 2022, made with by Creative Tim for a better web.
      </div>
    </div>
  );
}
