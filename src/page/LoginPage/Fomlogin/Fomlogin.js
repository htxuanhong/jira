import { message } from "antd";
import { useFormik } from "formik";
import React from "react";
import { useHistory } from "react-router-dom";
import { localStorageService } from "../../../service/localStorageService";
import { userService } from "../../../service/userService";
import "./FormLogin.css";

export default function Fomlogin() {
  let navigation = useHistory();
  const formik = useFormik({
    initialValues: {
      password: "",
      email: "",
    },
    onSubmit: (values) => {
      userService
        .postDangNhap(values)
        .then((res) => {
          console.log(res);
          localStorageService.setUserInfor(res.data.content.accessToken);
          message.success("Đăng nhập thành công");
          setInterval(() => {
            navigation.push("/");
          }, 3000);
        })
        .catch((err) => {
          console.log(err);
          message.error("Tài khoản hoặc mật khẩu không đúng");
        });
    },
  });
  return (
    <section className="">
      <div className="">
        <div className="card-body p-md-5 mx-md-4">
          <div className="text-center">
            <img
              src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/lotus.webp"
              alt="logo"
              className="w-40 h-20"
            />
            <h4 className="">We are The Lotus Team</h4>
          </div>
          <form onSubmit={formik.handleSubmit}>
            <p>Please login to your account</p>
            <div className="form-outline mb-4">
              <input
                type="email"
                onChange={formik.handleChange}
                value={formik.values.email}
                name="email"
                id="email"
                className="form-control"
                placeholder="Phone number or email address"
              />
              <label className="form-label" htmlFor="form2Example11">
                Username
              </label>
            </div>
            <div className="form-outline mb-4">
              <input
                onChange={formik.handleChange}
                value={formik.values.password}
                type="password"
                name="password"
                id="password"
                className="form-control"
                placeholder="••••••••"
              />
              <label className="form-label" htmlFor="form2Example22">
                Password
              </label>
            </div>
            <div className="text-center pt-1 mb-5 pb-1">
              <button
                className="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3"
                type="submit"
              >
                Log in
              </button>
              <a className="text-muted" href="#!">
                You must login with accout Jira to domain Cyberlear !
              </a>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
}
