import React from "react";
import Main from "../../component/Main";
import "../../index.css";

export default function HomePage() {
  return (
    <div>
      <Main />
    </div>
  );
}
