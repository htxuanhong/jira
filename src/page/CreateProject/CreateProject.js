// import { MessageTwoTone } from "@ant-design/icons";
import { message } from "antd";
import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";
import { setHideSpinner, setShowSpinner } from "../../redux/action/action";
import { projectService } from "../../service/projectService";
export default function CreateProject() {
  const login = "ACCESSTOKEN";
  const [state, setstate] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    projectService
      .getProjectCategory()
      .then((res) => {
        console.log(res);
        setstate(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let handleSlection = () => {
    return state.map((item, index) => {
      return (
        <option value={item.id} id={item.id}>
          {item.projectCategoryName}
        </option>
      );
    });
  };

  // formik lấy thông tin dữ liệu từ người dùng nhập vào
  const formik = useFormik({
    initialValues: {
      projectName: "",
      description: "",
      categoryId: "",
    },
    onSubmit: (values) => {
      dispatch(setShowSpinner());
      projectService
        .postCreateProjectAuthorization(values)
        .then((res) => {
          dispatch(setHideSpinner());
          console.log("gui len thanh cong", res);
          message.success("Thêm thành công");
        })
        .catch((err) => {
          dispatch(setHideSpinner());
          console.log(err);
          // message.error(err.response.data.content);
          message.error("Tạo không thành công , vui lòng tạo lại");
        });
      console.log(values);
    },
  });
  const handleEditorChange = (content, editor) => {
    // console.log("content", content); //
    // setFieldValue(" description", content);
  };
  if (!localStorage.getItem(login)) {
    return <Redirect to="/login" />;
  }
  return (
    <div className="h-screen">
      <div className="absolute h-screen ">
        <img
          src={require("../../assets/img/create.jpg")}
          className=""
          style={{ height: 892 }}
        />
      </div>
      <div className=" relative w-715 h-2/3 rounded-lg top-24 left-72 shadow-lg p-5  bg-white ">
        <h1 className="text-lg text-left font-medium text-gray-800">
          CreateProject
        </h1>
        <form onSubmit={formik.handleSubmit}>
          <div className="form-group">
            <label htmlFor="projectName">Name</label>
            <input
              id="projectName"
              name="projectName"
              type="text"
              className="form-control"
              placeholder="Project name"
              onChange={formik.handleChange}
              value={formik.values.projectName}
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlTextarea1">Description</label>

            <textarea
              id="description"
              name="description"
              type="text"
              className="form-control"
              placeholder="description"
              onChange={formik.handleChange}
              value={formik.values.description}
              rows="3"
            ></textarea>
          </div>

          <div className="form-group">
            <label htmlFor="projectName">Project Category</label>
            <select
              className="form-control"
              name="categoryId"
              id="categoryId"
              onChange={formik.handleChange}
              value={formik.values.categoryId}
            >
              {" "}
              {handleSlection()}
            </select>
          </div>

          <button
            type="submit"
            class="inline-block px-6 py-2.5 bg-blue-400 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
          >
            Create Project
          </button>
        </form>
      </div>
    </div>
  );
}
